/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.Serializable;

/**
 *
 * @author JA
 */
public class Time implements Serializable, Comparable<Time>{

    private int hour;
    private int minute;
    private boolean AM;

    public Time(int hour, int minute, boolean AM) {
        this.AM = AM;
        this.hour = hour;
        this.minute = minute;
        
    }
    
    public Time(int hour, int minute){
        this.hour=hour;
        this.minute=minute;
        if(hour<12) this.AM=true;
        else this.AM = false;
    }
    
    public Time(){
        this.hour=0;
        this.minute=0;
        this.AM=false;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setTime(int hour, int minute, boolean AM){
        this.AM = AM;
        if(AM){
        this.hour=hour;
        }else{
            this.hour=hour+12;
        }
        this.minute=minute;
        
    }
    
    @Override
    public String toString() {
        if(hour<10){
            if(minute<10) return "0"+hour + ":" + "0"+minute;
            else return "0"+hour + ":" +minute;
        }else{
            if(minute<10) return hour + ":" + "0"+minute;
            else return hour + ":" +minute;
        }
        
        
    }

    @Override
    public int compareTo(Time o) {
        if(this.hour<o.hour) return -1;
        else if(this.hour>o.hour) return 1;
        else{
            if(this.minute<o.minute) return -1;
            else if(this.minute>o.minute) return 1;
            else return 0;
        }
    }
    
    public boolean compareTo(int hour, int minute){
        if(this.hour%12==hour%12 && this.minute==minute) return true;
        else return false;
    }
    
    
    

}
