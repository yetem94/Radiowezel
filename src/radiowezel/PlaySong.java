/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.BufferedInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 *
 * @author Marcin
 */
public class PlaySong {

    private BufferedInputStream song;
    private Player p;
    private Thread thread;

    
    PlaySong(BufferedInputStream song) {
        this.song = song;
    }

    public void play() {      
            createThread();
            thread.start();
    }
    
    public void stop(){
        thread.stop();
    }

    public void setSong(BufferedInputStream song) {
        this.song = song;
    }

    private void createThread(){
        thread = new Thread() {
            @Override
            public void run() {
                    try {
                        p = new Player(song);
                        p.play();

                    } catch (JavaLayerException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        };
    }
    
    public boolean isAlive(){
        return thread.isAlive();
    }
    
    
    
}
