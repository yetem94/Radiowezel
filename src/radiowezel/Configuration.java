/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marcin
 */
public class Configuration implements Serializable {

    public File TemplateFile;
    public String layout;
    
    
    public Configuration() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("config.cfg"));
            Configuration conf = (Configuration) ois.readObject();
            this.TemplateFile = conf.TemplateFile;
            this.layout = conf.layout;
        } catch (StreamCorruptedException | FileNotFoundException | InvalidClassException | EOFException ex) {
            setDefault();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setDefault() {
        TemplateFile = new File("Nowy szablon.bin");
        layout = "Nimbus";
    }

    public void setTemplateFileToDefault() {
        TemplateFile = new File("Nowy szablon.bin");
    }
    
    public void setLayoutToDefault(){
        layout = "Nimbus";
    }
}
