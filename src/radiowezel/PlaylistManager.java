/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author JA
 */
public class PlaylistManager implements Serializable{
    private final Vector<Playlist> list;
    Thread player;

    public PlaylistManager() {
        list = new Vector<>();
        
        
        String root = System.getProperty("user.home")+"\\Music";
        File[] dirs = new File(root).listFiles();
        for(File file:dirs){
            if(file.isDirectory()) list.add(new Playlist(file.getPath()));
        }
    }

   public String[] getPlaylistNames(){
       String[] names = new String[list.size()];
       for(int i=0;i<list.size();i++){
           names[i]=list.get(i).getName();
       }
       return names;
   }
    
   
   public BufferedInputStream getRandomSong(String playlistName) throws FileNotFoundException{
       for(Playlist p: list){
           if(p.getName().equals(playlistName)) return p.getRandomSong();
       }
       return null;
   }
   
   public File getSong(String playlistName){
      for(Playlist p: list){
           if(p.getName().equals(playlistName)) return p.getSong();
       }
      return null;
   }
    
    
}
