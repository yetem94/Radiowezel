/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author JA
 */
public class Main extends javax.swing.JFrame {

    private PlaylistManager pm;
    private Template t;
    private AddEvent addEvent;
    private final Thread player;
    private Thread playSong;
    private Calendar cal;
    private BufferedInputStream song;
    private PlaySong playsong;
    private Configuration config;
    private Time actualTime;

    private final String fileExceptionMessage = "Błąd pliku";
    private final String noSelectEventExceptionMessage = "Nie wybrano wydarzenia";
    private final String playlistExceptionMessage = "Bład playlisty";

    /**
     * Creates new form Main
     *
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    public Main() throws IOException, FileNotFoundException, ClassNotFoundException {

        this.pm = new PlaylistManager();
        this.t = new Template();
        this.actualTime = new Time();
        initComponents();
        initConfiguration();

        playsong = new PlaySong(song);

        //Wątek odpowiedzialny za puszczanie muzyki.
        player = new Thread() {
            private int minute = 0;
            private boolean continius = false;
            private Event event;

            @Override
            public void run() {
                
                while (true) {
                  cal = Calendar.getInstance();
                   
                    //if (cal.get(Calendar.MINUTE) != minute) {
                        
                        //minute = cal.get(Calendar.MINUTE);
                        actualTime.setTime(cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), (cal.get(Calendar.AM_PM) == Calendar.AM));
                        event = t.checkStartTime(actualTime);
                        jMenu5.setText(actualTime.toString());

                        if (event != null) {
                            try {
                                song = pm.getRandomSong(event.getPlaylist());
                                playsong.setSong(song);
                                playsong.play();
                                continius = true;
                                updateState(event, pm.getSong(event.getPlaylist()));
                            } catch (FileNotFoundException ex) {
                                JOptionPane.showMessageDialog(rootPane, "Playlista którą próbuję odpalić jest pusta", playlistExceptionMessage, JOptionPane.ERROR_MESSAGE);
                            }
                        }
                        if (t.checkEndTime(actualTime)) {
                            playsong.stop();
                            continius = false;
                            updateState(null, null);
                        }
                    //}
                    if (continius && !playsong.isAlive()) {
                        try {
                            song = pm.getRandomSong(event.getPlaylist());
                            playsong.setSong(song);
                            playsong.play();
                            updateState(event, pm.getSong(event.getPlaylist()));
                        } catch (FileNotFoundException ex) {
                            JOptionPane.showMessageDialog(rootPane, "Playlista którą próbuję odpalić jest pusta", playlistExceptionMessage, JOptionPane.ERROR_MESSAGE);
                        }

                    }

                    if (cal.get(Calendar.MINUTE) % 5 == 0) {
                        saveConfiguration();
                    }
                    try {
                        sleep(60000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
               }
            }

        };
        player.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        eventList = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon(getClass().getResource("/assets/icon.png")).getImage());

        eventList.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista wydarzeń"));
        eventList.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        eventList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = t.toStringBoard();
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(eventList);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Obecny szablon"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Czekam na wydarzenie");
        jLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Obecne wydarzenie"));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setText("Czekam na wydarzenie");
        jLabel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Obecnie odtwarzane"));

        jMenuBar1.add(jMenu5);

        jMenu1.setText("Plik");

        jMenuItem6.setText("Nowy Szablon");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuItem1.setText("Zapisz Szablon");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem1MouseClicked(evt);
            }
        });
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Wczytaj Szablon");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edycja");

        jMenuItem3.setText("Dodaj wydarzenie");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("Edytuj wydarzenie");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem5.setText("Usuń wydarzenie");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);
        jMenu2.add(jSeparator1);

        jMenuItem8.setText("Otworz Playlisty");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem8);

        jMenuItem9.setText("Stworz Playliste");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem9);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Opcje");

        jMenuItem7.setText("Zapisz konfigurację");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MouseClicked

    }//GEN-LAST:event_jMenuItem1MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".bin") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".bin";
            }
        });
        chooser.showSaveDialog(null);
        File file = chooser.getSelectedFile();
        File newFile;
        if (file.getName().toLowerCase().endsWith(".bin")) {
            newFile = file;
        } else {
            newFile = new File(file.getAbsolutePath() + ".bin");
        }

        try {
            newFile.createNewFile();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, ex, fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }
        try {
            ObjectOutputStream ois = new ObjectOutputStream(new FileOutputStream(newFile));
            config.TemplateFile = newFile;
            jLabel2.setText(config.TemplateFile.getName());
            ois.writeObject(t);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(rootPane, "Nie mogę znaleźć takiego pliku", fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "Błąd w trakcie zapisywania danych", fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".bin") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".bin";
            }
        });
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            t = (Template) ois.readObject();
            refreshEventList();
            config.TemplateFile = file;
            jLabel2.setText(config.TemplateFile.getName());
        } catch (InvalidClassException ex) {
            JOptionPane.showMessageDialog(rootPane, "Ta wersja pliku nie jest kompatybilna z obecna wersją programu", fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "Ten plik nie pochodzi z tego programu. Nie mogę odczytać danych", fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(rootPane, "Nie znaleziono takiego pliku", fileExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        addEvent = new AddEvent(this);
        addEvent.show();
        this.disable();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        try {
            addEvent = new AddEvent(this, (Event) eventList.getSelectedValue());
            addEvent.show();
            this.disable();
            refreshEventList();
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(rootPane, "Musisz wybrać jakieś wydarzenie", noSelectEventExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        try {
            Event e = (Event) eventList.getSelectedValue();
            if (e == null) {
                throw new NullPointerException();
            }
            t.deleteEvent(e);
            refreshEventList();
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(rootPane, "Musisz wybrać jakieś wydarzenie", noSelectEventExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed

    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        saveConfiguration();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        String name = JOptionPane.showInputDialog("Podaj nazwę playlisty");
        File file = new File(System.getProperty("user.home")+"\\Music\\"+name);
        if(!file.mkdir()){
           JOptionPane.showMessageDialog(rootPane, "Playlista o podanej nazwie już istnieje", noSelectEventExceptionMessage, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        try {
            Runtime.getRuntime().exec("explorer "+System.getProperty("user.home")+"\\Music");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

            //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new Main().setVisible(true);
                } catch (IOException | ClassNotFoundException ex) {
                    System.out.println("coś poszło nie tak");
                }
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList eventList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    // End of variables declaration//GEN-END:variables

    public PlaylistManager getPm() {
        return pm;
    }

    public void addEvent(Event e) throws CollisionHourException {
        t.addEvent(e);
    }

    public void editEvent(Event old, Event newe) throws CollisionHourException {
        t.editEvent(old, newe);

    }

    public void refreshEventList() {
        eventList.setListData(t.getEvents());
    }

    private void initConfiguration() {
        config = new Configuration();

        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(config.TemplateFile.getAbsolutePath()));
            t = (Template) ois.readObject();
            eventList.setListData(t.getEvents());
        } catch (InvalidClassException | FileNotFoundException ex) {
            t = new Template();
            config.setTemplateFileToDefault();
        } catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        jLabel2.setText(config.TemplateFile.getName());

    }

    public void saveConfiguration() {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("config.cfg");
        } catch (FileNotFoundException ex) {
            try {
                File file = new File("config.cfg");
                file.createNewFile();
                fos = new FileOutputStream(file);
            } catch (IOException ex1) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(config);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateState(Event e, File song) {
        if (e == null) {
            jLabel1.setText("Czekam na wydarzenie");
            jLabel3.setText("Czekam na wydarzenie");
        } else {
            jLabel1.setText(e.toString());
            jLabel3.setText(song.getName().substring(0, song.getName().length() - 4));
        }
    }

}
