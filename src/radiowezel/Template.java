/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Marcin
 */
public class Template implements Serializable{
    private final Vector<Event> events;
    

    public Template() {
       events = new Vector<>();
    }

    public Vector<Event> getEvents() {
        Collections.sort(events);
        return events;
    }
    
    public void addEvent(Event e) throws CollisionHourException{
        for(Event event: events){
            if(event.collision(e)) throw new CollisionHourException();
        }
        events.add(e);
    }
    

    public String[] toStringBoard(){
        String[] string = new String[events.size()];
        for(int i=0;i<events.size();i++){
            string[i]=events.get(i).toString();
        }
        return string;
}

    
    public Event checkStartTime(Time time){
        for(Event event: events){
            if(event.compareStart(time.getHour(), time.getMinute())) return event;
        }
        return null;
    }
   
    public boolean checkEndTime(Time time){
        for(Event event: events){
            if(event.compareEnd(time.getHour(), time.getMinute())) return true;
        }
        return false;
    }
    
    public void deleteEvent(Event e){
        events.remove(e);
    }
    
    
    public void editEvent(Event old, Event newe) throws CollisionHourException{
        
        for(Event event: events){
            if(event.collision(newe)) throw new CollisionHourException();
        }
        events.remove(old);
        events.add(newe);
        Collections.sort(events);
    }
    
}


class CollisionHourException extends Exception{}