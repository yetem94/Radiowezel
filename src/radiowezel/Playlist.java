/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author JA
 */
public class Playlist implements Serializable {

    private final String path;
    private final String name;
    private final File[] songs;
    private Random ran;
    private int random;

    public Playlist(String path) {
        File f = new File(path);
        this.path = path;
        this.songs = f.listFiles();
        this.name = f.getName();
        this.ran = new Random();
        this.random = 0;
    }

    public String getName() {
        return name;
    }

    public BufferedInputStream getRandomSong() throws FileNotFoundException {
        this.random = ran.nextInt(songs.length);
        FileInputStream fis = new FileInputStream(songs[random]);
        return new BufferedInputStream(fis);
    }
    
    public File getSong(){
        return songs[random];
    }

}
