/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package radiowezel;

import java.io.Serializable;

/**
 *
 * @author JA
 */
public class Event implements Serializable, Comparable<Event>{
    private Time start;
    private Time end;
    private String playlist;

    public Event(Time start, Time end, String playlist) throws SameTimeException {
        if(start.compareTo(end)>=0) throw new SameTimeException();
        this.start = start;
        this.end = end;
        this.playlist = playlist;
    }

    public Time getStart() {
        return start;
    }

    public Time getEnd() {
        return end;
    }

    public String getPlaylist() {
        return playlist;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public void setEnd(Time end) {
        this.end = end;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    @Override
    public String toString() {
        return start + " " + end + " " + playlist;
    }

    @Override
    public int compareTo(Event o) {
        return this.start.compareTo(o.start);
    }
    
    public boolean collision(Event o){
        return (o.start.compareTo(this.start)>=0 && o.start.compareTo(this.end)<=0) || (o.end.compareTo(this.start)>=0 && o.end.compareTo(this.end)<=0);
    }
    
    public boolean compareStart(int hour, int minute){
        return this.start.compareTo(hour, minute);
    }
    
    public boolean compareEnd(int hour, int minute){
        return this.end.compareTo(hour, minute);
    }
    
    
}

class SameTimeException extends Exception{}